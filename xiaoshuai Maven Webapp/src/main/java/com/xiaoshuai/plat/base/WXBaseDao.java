package com.xiaoshuai.plat.base;

import com.xiaoshuai.plat.model.WXUserInfo;

public interface WXBaseDao {
	public void save(WXUserInfo paramWXUserInfo) throws Exception;

	public void insert(WXUserInfo paramWXUserInfo) throws Exception;

	public void update(WXUserInfo paramWXUserInfo) throws Exception;

	public WXUserInfo getByOpenId(String paramString) throws Exception;
}
