package com.xiaoshuai.plat.vo;
/**
 * 按钮的基类
 * @author 宗潇帅
 * @修改日期 2014-7-14上午10:25:42
 */
public class Button {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
